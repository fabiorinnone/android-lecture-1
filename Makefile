# Makefile per presentazione in LaTeX

FN = 'lecture-1'

default: all

all:
	pdflatex $(FN).tex
	pdflatex $(FN).tex
		
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.class
	rm -f *.bcf
	rm -f *.out
	rm -f *.nav
	rm -f *.snm
	rm -f *.run.xml
	rm -f *~

erase:
	make clean
	rm -f *.dvi*
	rm -f *.ps*
	rm -f *.pdf*

help:
	@echo "make [all]: genera i file .dvi e .pdf di tutti i file .tex"
	@echo "make wikitim-lez1: genera i file .dvi e .pdf " 
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e i file .dvi, .ps e .pdf"
	@echo "Leggere anche README"
